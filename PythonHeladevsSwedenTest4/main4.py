try : 
    def val_power(int_val : int, int_power : int) -> int :
        if (int_power == 0) :
            return 1

        return int_val * val_power(int_val, int_power - 1)

    val = input("Enter the number (Eg :- 23 = 8) :- ")

    if(len(val) != 2) :
        print("Invalid Details")

        exit()

    int_val = int(val[0])
    int_power = int(val[1])

    print(val_power(int_val, int_power))
except ValueError as e:
    print("Invalid Number Format")