import re

def count_vowel(input : str) -> int:
    return len(re.findall("a|e|i|o|u|A|E|I|O|U", input))

input = input("Enter the string :- ")

print(count_vowel(input))