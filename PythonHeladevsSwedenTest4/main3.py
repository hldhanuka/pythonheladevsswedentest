###################### old code #############################

# from datetime import datetime

# print("Instructions --- ")

# print("     Enter the DOB with this date format as a first input (MM/DD/YYYY) :- 1994-11-07")
# print("     Enter the Name as a second input :- Dhanuka")
# print("     Use ',' as a sperator")
# print("     Use 'exit' command to finish the inputs")

# oldest_student_name = ""
# oldest_student_dob = ""
# input_lable = "Enter DOB and Name : "
# input_val = input(input_lable)

# # validate the dob
# def validate_dob (dob : str) -> datetime | bool :
#     try :
#         return datetime.strptime(dob, '%Y-%m-%d')
#     except ValueError as e:
#         # print(e)

#         return False

# # print the dob and name
# def pirnt_dob_name() -> None :
#     print(f"oldest student name :- {oldest_student_name}")

# # common function to call the inputs
# def call_input() -> None :
#     input_val = input(input_lable)

#     return get_oldest_student(input_val)

# # get the olders student name and print
# def get_oldest_student(input_val : str) -> None :
#     global oldest_student_name
#     global oldest_student_dob

#     if(input_val == 'exit') :
#         pirnt_dob_name()

#         print("Closing the programme")

#         exit()

#     # seperate dob and name
#     dob_name = input_val.split(',')

#     # validate input length
#     if (len(dob_name) != 2) :
#         print("There should be ',' seperated 2 values")

#         return call_input()

#     # assing the details of input
#     dob_object = validate_dob(dob_name[0])
#     name_object = dob_name[1]

#     # check dob validation
#     if not dob_object :
#         print("Invalid DOB")

#         return call_input()

#     # check assinged details and oldest dob
#     if (not oldest_student_dob) :
#         oldest_student_dob = dob_object 
#         oldest_student_name = name_object
#     elif ((oldest_student_dob > dob_object)) :
#         oldest_student_dob = dob_object 
#         oldest_student_name = name_object

#     pirnt_dob_name()

#     return call_input()

# get_oldest_student(input_val)

###################### old code #############################

from datetime import datetime

print("Instructions --- ")

print("     Enter the DOB with this date format as a first input (MM/DD/YYYY) :- 1994-11-07")
print("     Enter the Name as a second input :- Dhanuka")
print("     Use ',' as a sperator")
print("     Use 'exit' command to finish the inputs")

oldest_student_name = ""
oldest_student_dob = ""
input_lable = "Enter DOB and Name : "
input_val = input(input_lable)

# validate the dob
def validate_dob (dob : str) -> datetime | bool :
    try :
        return datetime.strptime(dob, '%Y-%m-%d')
    except ValueError as e:
        # print(e)

        return False

# print the dob and name
def pirnt_dob_name(student_name : str) -> None :
    print(f"oldest student name :- {student_name}")

# common function to call the inputs
def call_input() -> None :
    input_val = input(input_lable)

    return get_oldest_student(input_val)

# get the olders student name and print
def get_oldest_student(input_val : str) -> None :
    # global oldest_student_name
    # global oldest_student_dob

    if(input_val == 'exit') :
        pirnt_dob_name()

        print("Closing the programme")

        exit()

    # seperate dob and name
    dob_name = input_val.split(',')

    # validate input length
    if (len(dob_name) != 2) :
        print("There should be ',' seperated 2 values")

        return call_input()

    # assing the details of input
    dob_object = validate_dob(dob_name[0])
    name_object = dob_name[1]

    # check dob validation
    if not dob_object :
        print("Invalid DOB")

        return call_input()

    # check assinged details and oldest dob
    if (not oldest_student_dob) :
        oldest_student_dob = dob_object 
        oldest_student_name = name_object
    elif ((oldest_student_dob > dob_object)) :
        oldest_student_dob = dob_object 
        oldest_student_name = name_object

    pirnt_dob_name(oldest_student_name)

    return call_input()

get_oldest_student(input_val)