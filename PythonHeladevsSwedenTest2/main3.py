'''

Prompt user to provide a positive integer, Then print the multiplication table upto 10

    Enter an integer: 3
    3 x 1 = 3
    3 x 2 = 6
    3 x 3 = 9
    3 x 4 = 12
    3 x 5 = 15
    3 x 6 = 18
    3 x 7 = 21
    3 x 8 = 24
    3 x 9 = 27
    3 x 10 = 30

'''
try:
    input = int(input("Enter A Number :- "))

    if input <= 0 :
        print("Please enter greather than 0 value")
    else :
        for i in range(1, 11) :
            print(f"{input} x {i} = {input * i}")
except ValueError:
    print("Invalid Entries")