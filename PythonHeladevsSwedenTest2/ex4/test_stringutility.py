import unittest
import stringutility

class TestStringUtility(unittest.TestCase) :
    def test_total(self) :
        number_list = ["Apple", "Orage"]

        expected_output = 1

        actual_output = stringutility.count_by_starting_character(number_list, "A")

        self.assertEqual(expected_output, actual_output)

if __name__ == '__main__' :
    unittest.main()