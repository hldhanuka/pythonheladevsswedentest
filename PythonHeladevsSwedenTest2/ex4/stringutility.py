def count_by_starting_character(list, character) -> int :
    """ Return the count of strings starting with the given character """
    count = 0

    for val in list :
        if val.startswith(character) :
            count += 1

    return count