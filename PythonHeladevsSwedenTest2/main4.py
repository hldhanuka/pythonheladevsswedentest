'''

Get 10 names from user inputs, count the names start with B and display those names at the end.

'''

names = []

for x in range(1, 11):
    name = input(f"Enter A Name {x} :- ")

    # get first letter
    if ("B" == name[0]) :
        names.append(name)

print(f"Count :- {len(names)}")

print("Name List :- ")

for name in names :
    print(f"{name}")