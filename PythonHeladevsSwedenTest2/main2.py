'''
You are given an list as below, Calculate the total, average and maximum, minimum values in the list using a proper looping technique

    numbers = [45, 93, 34, 71, 89, 56]

'''

numbers = [45, 93, 34, 71, 89, 56]

total = 0
max_val = 0
min_val = 0

for i in numbers:
    # calculate the total
    total += i

# Sorting list
numbers.sort()

# calculate the max value
max_val = numbers[-1]

# calculate the min value
min_val = numbers[0]

print(f"Total :- {total}")
print(f"Average :- {total/len(numbers)}")
print(f"Max Val :- {max_val}")
print(f"Min Val :- {min_val}")