import numberutility

numbers = [45, 93, 34, 71, 89, 56]

total = numberutility.total(numbers)
average = numberutility.average(numbers)
max = numberutility.max(numbers)
min = numberutility.min(numbers)

print(f"Total :- {total}")
print(f"Average :- {average}")
print(f"Max Val :- {max}")
print(f"Min Val :- {min}")