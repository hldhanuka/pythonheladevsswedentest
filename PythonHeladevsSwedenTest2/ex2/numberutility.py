"""
This module provides methods for basci number operations
"""

def total(list) -> int :
    """ Calculates sum of the given numbers in the list """
    total_val = 0

    for number in list :
        total_val += number

    return total_val

def average(list) -> float:
    """ Calculates the average of the given list"""
    return total(list) / len(list)

def max(list) -> int:
    """ Retrun the max value of a given list """
    max_value = list[0]

    for number in list :
        if number > max_value :
            max_value = number

    return max_value

def min(list) -> int:
    """ Retrun the min value of a given list """
    min_value = list[0]

    for number in list :
        if number < min_value :
            min_value = number

    return min_value
