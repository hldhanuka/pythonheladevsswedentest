import unittest
import numberutility

class TestNumberUtility(unittest.TestCase) :
    def test_total(self) :
        number_list = [1, 2, 3]

        expected_total = 6

        actual_output = numberutility.total(number_list)

        self.assertEqual(expected_total, actual_output)

    def test_average(self) :
        number_list = [1, 2, 3]

        excepted_avg = 2

        actual_avg = numberutility.average(number_list)

        self.assertEqual(excepted_avg, actual_avg)

    def test_max(self) :
        number_list = [1, 2, 3]

        excepted_max = 3

        actual_max = numberutility.max(number_list)

        self.assertEqual(excepted_max, actual_max)

    def test_min(self) :
        number_list = [1, 2, 3]

        excepted_min = 1

        actual_min = numberutility.min(number_list)

        self.assertEqual(excepted_min, actual_min)

if __name__ == '__main__' :
    unittest.main()