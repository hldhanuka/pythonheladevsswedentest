import random
import list_ascending_descending

random_list = random.sample(range(0, 1000), 100)

print(f"random list :- {random_list}")

random_list.sort()

print(f"ascending list :- {random_list}")

random_list.sort(reverse = True)

print(f"descending list :- {random_list}")

###### ---------------------------------------------------------------------------------------------

print("\n")

self_creating_random_list = random.sample(range(0, 1000), 100)

print(f"random list :- {self_creating_random_list}")

self_crated_ascending_ist = list_ascending_descending.list_ascending_descending(self_creating_random_list, "ascending")

print(f"self created ascending list :- {self_crated_ascending_ist}")

self_crated_descending_ist = list_ascending_descending.list_ascending_descending(self_creating_random_list, "descending")

print(f"self created descending list :- {self_crated_descending_ist}")