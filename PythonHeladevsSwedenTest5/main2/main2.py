all_student = []
TERAMS = ["Tearm 1", "Tearm 2", "Tearm 3"]
SUBJECT_LIST = ["Maths", "Science", "Art"]

def display_main_menu() -> None :
    print("1 - Enter New Student")
    print("2 - Enter The Tearm and Wll show who has the maximum average as well as the lowest maks for the Science")

    print("# - Exit")

def get_student_details_max_average(teram : int) -> dict : 
    student_details = {}
    lowest_marks_student = {}
    best_average = 0
    lowset_marks_science = 100

    for student in all_student :
        tearms_wise_average = student[TERAMS[teram - 1]]
        average = tearms_wise_average['average']
        science_marks = tearms_wise_average[SUBJECT_LIST[1]]

        if(average > best_average) :
            student_details = student
            best_average = average

        if(lowset_marks_science > science_marks) :
            lowset_marks_science = science_marks
            lowest_marks_student = student

    return {"best_tearm_average_studen_details" : student_details, "lowest_marks_science_studen_details" : lowest_marks_student}

def get_terms_subject_details(studen_details : dict) -> dict :
    for tearm in TERAMS:
        subjects_details = {}
        total = 0

        for subject in SUBJECT_LIST:
            marks =  float(input(f"Enter {tearm} {subject} marks : "))

            subjects_details[subject] = marks
            total += marks

        studen_details[tearm] = subjects_details
        studen_details[tearm]["total"] = total
        studen_details[tearm]["average"] = float(total / len(SUBJECT_LIST))

    return studen_details

def get_student_details_terms() -> dict:
    studen_details = {}

    student_name =  input("Enter Student Name: ")

    studen_details['name'] = student_name

    student_grade =  input("Enter Student Grade: ")

    studen_details['grade'] = student_grade

    student_class =  input("Enter Student Class: ")

    studen_details['class'] = student_class

    return studen_details

while True :
    display_main_menu()

    choice = input("Please enter your choice : ")

    if choice == "1":
        student_details = get_student_details_terms()

        student_details = get_terms_subject_details(student_details)

        all_student.append(student_details)

        print(all_student)
    elif choice == "2":
        teram = int(input("Enter The Tearm: "))

        student_details_max_average = get_student_details_max_average(teram)

        best_tearm_average_student_name = student_details_max_average['best_tearm_average_studen_details']['name']
        lowest_marks_science_studen_name = student_details_max_average['lowest_marks_science_studen_details']['name']

        print(f"Name of the best tearm average student :- {best_tearm_average_student_name}")
        print(f"Name of the student who has the lowest marks for the Science :- {lowest_marks_science_studen_name}")
    elif choice == "#":
        exit()
    else :
        exit()
