
def list_ascending_descending(self_creating_random_list : list, type : str) -> list :
    sorted_list = []

    if (type == "ascending") :
        for val in self_creating_random_list :
            sub_index = 0
            tmp_sorted_list = sorted_list

            if not sorted_list:
                tmp_sorted_list.append(val)
            else :
                for sub_val in sorted_list :
                    if(sub_val > val) :
                        tmp_sorted_list.insert(sub_index, val)

                        sorted_list = tmp_sorted_list

                        break
                    else :
                        if ((len(sorted_list) - 1) == sub_index) :
                            tmp_sorted_list.append(val)

                            sorted_list = tmp_sorted_list

                            break

                    sub_index += 1
    else :
        for val in self_creating_random_list :
            sub_index = 0
            tmp_sorted_list = sorted_list

            if not sorted_list:
                tmp_sorted_list.append(val)
            else :
                for sub_val in sorted_list :
                    if(sub_val < val) :
                        tmp_sorted_list.insert(sub_index, val)

                        sorted_list = tmp_sorted_list

                        break
                    else :
                        if ((len(sorted_list) - 1) == sub_index) :
                            tmp_sorted_list.append(val)

                            sorted_list = tmp_sorted_list

                            break

                    sub_index += 1

    return sorted_list