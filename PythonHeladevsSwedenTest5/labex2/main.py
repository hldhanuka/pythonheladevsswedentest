def get_marks() -> tuple :
    maths = int(input("Enter marks for Maths :- "))
    science = int(input("Enter marks for Science :- "))
    arts = int(input("Enter marks for Arts :- "))

    return (maths, science, arts)

def get_marks_for_each_term() -> tuple:
    terms = []

    for term_number in range(1, 4):
        print(f"Enter marks for term : {term_number}")

        term = get_marks()

        terms.append(term)

    return tuple(terms)

def get_student_details() -> tuple:
    name = input("Ener student name :")
    grade = input("Ener student grade :")
    s_class = input("Ener student class :")

    terms = get_marks_for_each_term()

    print("-----------------------------------------------------------------")

    return (name, grade, s_class, terms)

student_details = []

while True:
    student = get_student_details()

    student_details.append(student)

    user_iput = input("Do you want to continue Y/N ?")

    if user_iput == "N":
        break

print(student_details)