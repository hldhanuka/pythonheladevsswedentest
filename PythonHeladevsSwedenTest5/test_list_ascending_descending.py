import unittest
import list_ascending_descending
import random

class TestListAscendingDescending(unittest.TestCase) :
    def test_list_ascending_descending(self) :
        self_creating_random_list = random.sample(range(0, 1000), 100)

        result = list_ascending_descending.list_ascending_descending(self_creating_random_list, "ascending")

        self_creating_random_list.sort()

        self.assertEqual(result, self_creating_random_list)

if __name__ == '__main__' :
    unittest.main()