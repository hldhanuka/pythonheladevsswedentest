try:
    value = int(input(f"Enter Value :- "))

    tempMethod = input("Enter the unit celcious or fahrenheit (C/F) :- ")

    if ("C" == tempMethod) :
        value = round((9 * value) / 5 + 32, 2)

        print(f"Temperature in celcious : {value}")
    elif ("F" == tempMethod) :
        value = round((value - 32) * 5 / 9, 2)

        print(f"Temperature in fahrenheit : {value}")
    else: 
        print("Input proper convention.")
except ValueError:
    print("Invalid Entries")