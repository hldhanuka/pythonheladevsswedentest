class DataError(Exception):
    pass

class CalculationError(Exception):
    pass

def get_number_list(file_name : str) -> list:
    try:
        with open(file_name, "r") as datafile:
            return datafile.readlines()
    except FileNotFoundError:
        error_message = "Coud not fine the file"

        raise DataError(error_message)
    except PermissionError:
        error_message = "Could not open the file due to lack of permissions"

        raise DataError(error_message)
    except OSError:
        error_message = "Could not open the file due to some errors"

        raise DataError(error_message)

def calculate_sum(number_list : list) -> int:
    total = 0

    try:
        for number in number_list:
            total += int(number)
    except ValueError:
        raise CalculationError("Incorrect value found! Please fix the input")

    return total

try:
    file_name = "datafile.txt"

    numbers = get_number_list(file_name)

    total = calculate_sum(numbers)

    print(f"Sum : {total}")
except DataError as d_error:
    print(d_error)
except CalculationError as c_error:
    print(c_error)