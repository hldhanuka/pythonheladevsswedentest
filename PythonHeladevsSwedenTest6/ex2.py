filename = 'test_files\\ex2.txt'

def display_menu() :
    print("Simple List")

    print("1 - Enter the Input")
    print("2 - Show the list")
    print("3 - Exit")

while True :
    display_menu()

    choice = int(input("Please enter your choice : "))

    if choice == 1:
        item_name = input("Enter the Input : ")

        with open(filename, 'a') as f:
            f.write(item_name + "\n")
    elif choice == 2:
        txt_file = open(filename, "r")

        for row in txt_file :
            print(row.strip())

        txt_file.close()
    elif choice == 3:
        exit()
    else :
        display_menu()