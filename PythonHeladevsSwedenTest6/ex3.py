data_array = [1, 2, 3, 4, 5]
filename = "test_files\\ex3.bin"

with open(filename, 'wb') as f:
    for data in data_array:
        row_data = str(data) + "\n"

        f.write(row_data.encode('ascii'))

binary_file = open(filename, "rb")

for row in binary_file :
    print(row.strip().decode('utf-8'))

binary_file.close()
