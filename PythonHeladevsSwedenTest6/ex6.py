filename = 'test_files\\ex6.csv'

def extract_data_value(row : str) -> list:
    elements = row.split(",")

    return elements

def concatinate_data_value(row : list) -> str:
    elements = ','.join(row)

    return elements

def display_menu() :
    print("Users Details")

    print("1 - Enter the Details")
    print("2 - Show the Details List")
    print("3 - Exit")

while True :
    display_menu()

    choice = int(input("Please enter your choice : "))

    if choice == 1:
        name = input("Enter Name : ")
        age = input("Enter Age : ")
        tele = input("Enter Telephone : ")

        with open(filename, 'a') as f:
            f.write(concatinate_data_value([name, age, tele]) + "\n")
    elif choice == 2:
        csv_file = open(filename, "r")

        for row in csv_file :
            row_csv_data = extract_data_value(row.strip())

            csv_name = row_csv_data[0]
            csv_age = row_csv_data[1]
            csv_tele = row_csv_data[2]

            print(f"Name :- {csv_name} \n")
            print(f"Age :- {csv_age} \n")
            print(f"Telephone :- {csv_tele} \n")
            print("\n")

        csv_file.close()
    elif choice == 3:
        exit()
    else :
        display_menu()