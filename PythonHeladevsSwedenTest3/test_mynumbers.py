import unittest
import mynumbers

class TestMyNumbers(unittest.TestCase) :
    def test_total(self) :
        result = mynumbers.total(10, 30)

        expected_result = 40

        self.assertEqual(result, expected_result)

if __name__ == '__main__' :
    unittest.main()