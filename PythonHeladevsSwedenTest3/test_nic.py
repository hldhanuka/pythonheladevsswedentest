import unittest
import nic

class TestNic(unittest.TestCase) :
    def test_nic_men(self) :
        nic_val = "197900209871"

        result = nic.get_born_date(nic_val)

        born_year = result.year
        born_month = result.month
        born_day = result.day

        self.assertEqual(born_year, 1979)
        self.assertEqual(born_month, 1)
        self.assertEqual(born_day, 2)

    def test_nic_women(self) :
        nic_val = "197950209871"

        result = nic.get_born_date(nic_val)

        born_year = result.year
        born_month = result.month
        born_day = result.day

        self.assertEqual(born_year, 1979)
        self.assertEqual(born_month, 1)
        self.assertEqual(born_day, 2)

if __name__ == '__main__' :
    unittest.main()